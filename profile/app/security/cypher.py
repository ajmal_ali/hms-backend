from cryptography.fernet import Fernet

def generate_key():
    key = Fernet.generate_key()
    key = key.decode('utf-8')
    return key