from passlib.context import CryptContext

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

SECRET_KEY = "f669565906612024c22d74227d01f8f41abbb3b351bd20a61977374d92faf555"

ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 300

def get_password_hash(password):
    return pwd_context.hash(password)

def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)
