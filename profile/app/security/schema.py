from pydantic import BaseModel
from typing import Optional, List

class Token(BaseModel):
    access_token: str
    data : dict

class TokenData(BaseModel):
    username: Optional[str] = None
    scopes: List[str] = []

class Authdetails(BaseModel):
    username : str
    password: str
    # user_type: Optional[str]
