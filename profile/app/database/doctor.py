from bson.objectid import ObjectId
from app.database.config import users_coll

from app.security.security import get_password_hash

from app.models.doctor import (
    doctor_user_helper,
    doctor_user_list_helper,
    doctor_user_search_helper,
    doctor_user_scope
)

async def add_doctor_user_data_db(data: dict) -> dict:
    data['password'] = get_password_hash(data['password'])
    # data['profile_pic'] = None
    data['status'] = 'completed'
    data['user_type'] = 'doctor'
    data['scope'] = doctor_user_scope
    user = await users_coll.insert_one(data)
    new_user = await users_coll.find_one({"_id":user.inserted_id})
    return doctor_user_helper(new_user)

async def update_doctor_data_db(id: str, data: dict):
    if not data:
        return False
    data = {k: v for k, v in data.items() if v is not None}

    if "password" in data.keys():
        if data["password"]!="None":
            data["password"] = get_password_hash(data["password"])
        else:
            data.pop("password", None)

    doctor_user_data = await users_coll.find_one({"_id": ObjectId(id)})
   
    if doctor_user_data:
        updated_user_data = await users_coll.update_one(
            {"_id": ObjectId(id)}, {"$set": data}
        )
        if updated_user_data:
            return True
        return None
    return False

# Retrieve a user_data with a matching ID
async def retrieve_doctor_user_data_db(id: str) -> dict:
    user_data = await users_coll.find_one({"_id": ObjectId(id)})
    if user_data:
        # if user_data['status'] == 'completed':
            return doctor_user_helper(user_data)
        # return False
    return False

async def retrieve_doctors_users_list_db():
    user_list = []
    async for doctor in users_coll.find({'status': 'completed', 'user_type': 'doctor'}, {'password': 0}):
        user_list.append(doctor_user_list_helper(doctor))
    return user_list

async def search_doctor_name_db(search_para):
    resp_data = []
    async for user_data in users_coll.find({'$text': { '$search': search_para }}, {'password': 0}):
        user_data = doctor_user_search_helper(user_data)
        user_data['data_type'] = 'eagle_user'
        resp_data.append(user_data)
    return resp_data