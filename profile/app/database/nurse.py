from bson.objectid import ObjectId
from app.database.config import users_coll

from app.security.security import get_password_hash

from app.models.nurse import (
    nurse_user_helper,
    nurse_user_list_helper,
    nurse_user_search_helper,
    nurse_user_scope
)

async def add_nurse_user_data_db(data: dict) -> dict:
    data['password'] = get_password_hash(data['password'])
    # data['profile_pic'] = None
    data['status'] = 'completed'
    data['user_type'] = 'nurse'
    data['scope'] = nurse_user_scope
    user = await users_coll.insert_one(data)
    new_user = await users_coll.find_one({"_id":user.inserted_id})
    return nurse_user_helper(new_user)

async def update_nurse_data_db(id: str, data: dict):
    if not data:
        return False
    data = {k: v for k, v in data.items() if v is not None}

    if "password" in data.keys():
        if data["password"]!="None":
            data["password"] = get_password_hash(data["password"])
        else:
            data.pop("password", None)

    nurse_user_data = await users_coll.find_one({"_id": ObjectId(id)})
   
    if nurse_user_data:
        updated_user_data = await users_coll.update_one(
            {"_id": ObjectId(id)}, {"$set": data}
        )
        if updated_user_data:
            return True
        return None
    return False

# Retrieve a user_data with a matching ID
async def retrieve_nurse_user_data_db(id: str) -> dict:
    user_data = await users_coll.find_one({"_id": ObjectId(id)})
    if user_data:
        # if user_data['status'] == 'completed':
            return nurse_user_helper(user_data)
        # return False
    return False

async def retrieve_nurses_users_list_db():
    user_list = []
    async for nurse in users_coll.find({'status': 'completed', 'user_type': 'nurse'}, {'password': 0}):
        user_list.append(nurse_user_list_helper(nurse))
    return user_list

async def search_nurse_name_db(search_para):
    resp_data = []
    async for user_data in users_coll.find({'$text': { '$search': search_para }}, {'password': 0}):
        user_data = nurse_user_search_helper(user_data)
        user_data['data_type'] = 'eagle_user'
        resp_data.append(user_data)
    return resp_data