from bson.objectid import ObjectId

from app.database.config import users_coll

from app.models.login import user_login_helper

from app.security.security import get_password_hash


async def retrieve_user_with_phone_no(phone_no: str):
    user = await users_coll.find_one({'phone_no': phone_no})
    if user:
        return user_login_helper(user)
    return None

async def retrieve_user_with_email_id(email_id: str):
    user = await users_coll.find_one({'email_id': email_id})
    if user:
        return user_login_helper(user)
    return None

async def update_user_password_db(id: str, user_type: str, data: dict):

    data["password"] = get_password_hash(data["password"])

    user_data = await users_coll.find_one({"_id": ObjectId(id)})
   
    if user_data:
        updated_user_data = await users_coll.update_one(
            {"_id": ObjectId(id), "user_type": user_type}, {"$set": data}
        )
        if updated_user_data:
            return True
        return None
    return False