from bson.objectid import ObjectId
from app.database.config import users_coll

from app.security.security import get_password_hash

from app.models.patient import (
    patient_user_helper,
    patient_user_list_helper,
    patient_user_search_helper,
    patient_user_scope
)

async def add_patient_user_data_db(data: dict) -> dict:
    data['password'] = get_password_hash(data['password'])
    data['status'] = 'completed'
    data['user_type'] = 'patient'
    data['scope'] = patient_user_scope
    user = await users_coll.insert_one(data)
    new_user = await users_coll.find_one({"_id":user.inserted_id})
    return patient_user_helper(new_user)

async def update_patient_data_db(id: str, data: dict):
    if not data:
        return False
    data = {k: v for k, v in data.items() if v is not None}

    if "password" in data.keys():
        if data["password"]!="None":
            data["password"] = get_password_hash(data["password"])
        else:
            data.pop("password", None)

    patient_user_data = await users_coll.find_one({"_id": ObjectId(id)})
   
    if patient_user_data:
        updated_user_data = await users_coll.update_one(
            {"_id": ObjectId(id)}, {"$set": data}
        )
        if updated_user_data:
            return True
        return None
    return False

# Retrieve a user_data with a matching ID
async def retrieve_patient_user_data_db(id: str) -> dict:
    user_data = await users_coll.find_one({"_id": ObjectId(id)})
    if user_data:
        # if user_data['status'] == 'completed':
            return patient_user_helper(user_data)
        # return False
    return False

async def retrieve_patients_users_list_db():
    user_list = []
    async for patient in users_coll.find({'user_type': 'patient'}):
        user_list.append(patient_user_list_helper(patient))
    return user_list

async def search_patient_name_db(search_para):
    resp_data = []
    async for user_data in users_coll.find({'$text': { '$search': search_para }}, {'password': 0}):
        user_data = patient_user_search_helper(user_data)
        user_data['data_type'] = 'eagle_user'
        resp_data.append(user_data)
    return resp_data