import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from app.mail.email_config import (
    mail_id,
    password,
    smtp_server,
    port
)

def send_mail(mail_body):
    
    msg = MIMEMultipart()
    msg['From'] = mail_id
    msg['To'] = mail_body['to_addr']
    msg['Subject'] = mail_body['subject']
    msg.attach(MIMEText(mail_body['message'], 'plain'))

    # try:
    with smtplib.SMTP_SSL(smtp_server,port) as smtp:
        smtp.login(mail_id, password)
        smtp.send_message(msg)
        smtp.quit()
        return True
    # except:
    #     return False