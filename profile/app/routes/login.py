from fastapi import APIRouter, Body, Depends, Form, status
from fastapi.exceptions import HTTPException


from app.security.auth import authenticate_user, createJWT, decodeJWT, JWTBearer
from app.security.schema import Authdetails

from app.mail.email_functions import send_mail

from app.database.login import (
    retrieve_user_with_email_id,
    retrieve_user_with_phone_no,
    update_user_password_db
)

router = APIRouter()

@router.post("/test", status_code=status.HTTP_200_OK)
async def test_post_request(authdetails: Authdetails = Body(...)):
    return {
        "id": "452189",
        "usertype": "doctor",
        "username": authdetails.username
    }

@router.post("/login", status_code=status.HTTP_200_OK)
async def get_access_token(authdetails: Authdetails = Body(...)):
    if '@' in str(authdetails.username):
        user_data = await retrieve_user_with_email_id(authdetails.username)
    else:
        user_data = await retrieve_user_with_phone_no(authdetails.username)
    if user_data:
        flag = authenticate_user(authdetails, user_data["password"])
        if flag:
            user_data["access_token"] = createJWT(
                user_name=None, 
                scope=user_data['scope'], 
                id=user_data['id'],
                user_phone_no=user_data['phone_no']
            )
            return user_data
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Incorrect username or password")
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User data not found")

@router.post("/reset/password/onetimelinkgen", status_code=status.HTTP_200_OK)
async def generate_one_time_link(email_id: str = Form(...)):
    user_data = await retrieve_user_with_email_id(email_id)
    if user_data:
        token = createJWT(
            user_name=None,
            scope={
                "password": ['update']
            },
            id = user_data['id'],
            user_email=email_id,
            user_type=user_data['user_type']
        )
        flag = send_mail(mail_body={
            'to_addr': email_id,
            'subject': 'Password Reset Link',
            'message': 'http://localhost:8000/reset/password/' + token # webhook link of frontent
        })
        if flag:
            return {
                'detail': 'Verification link send successfully'
            }
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Email not send")
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Email Id not found")

@router.post("/reset/password", status_code=status.HTTP_200_OK)
async def update_with_new_password(password: str = Form(...), token: str = Depends(JWTBearer(user_type=["password","update"]))):
    payload = decodeJWT(token)
    resp = await update_user_password_db(id=payload['id'], user_type=payload['user_type'], data={'password': password})
    if resp:
        return {
            'detail': 'Password reseted successfully'
        }
    raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Password cannot be updated")