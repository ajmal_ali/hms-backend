from base64 import decode
from fastapi import APIRouter, Body, Depends, status, UploadFile, File, WebSocket, WebSocketDisconnect
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import HTTPException
from fastapi.param_functions import Form
from jwt import DecodeError

from app.security.auth import JWTBearer, decodeJWT

from app.models.doctor import (
    AddUserDoctorDetails,
    UpdateUserDoctorDetails
)

from app.database.doctor import (
    retrieve_doctor_user_data_db,
    add_doctor_user_data_db,
    update_doctor_data_db,
    retrieve_doctors_users_list_db,
    search_doctor_name_db
)

from app.database.login import (
    retrieve_user_with_email_id,
    retrieve_user_with_phone_no
)

router = APIRouter()

@router.get('/get/user/notifications/', status_code=status.HTTP_200_OK)
async def retrieve_doctor_user_summary(token: str = Depends(JWTBearer(user_type=["doctor","notification"]))): # user_id: str,
    pass

@router.get('/get/user/summary/', status_code=status.HTTP_200_OK)
async def retrieve_doctor_user_notification(token: str = Depends(JWTBearer(user_type=["doctor","view"]))):
    pass

@router.get('/get/user/data/', status_code=status.HTTP_200_OK)
async def retrieve_doctor_user_data(token: str = Depends(JWTBearer(user_type=["doctor","view"]))):
    payload = decodeJWT(token)
    resp_data = await retrieve_doctor_user_data_db(id=payload['id'])
    if resp_data:
        return resp_data
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User data not found")

@router.get('get/user/list/', status_code=status.HTTP_200_OK)
async def get_doctor_users_list(token: str = Depends(JWTBearer(user_type=["doctor","list"]))):
    try:
        users_data = await retrieve_doctors_users_list_db()
        return users_data
    except:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Users list retrieval failed")

@router.get('/get/user/list/search/{search_para}', status_code=status.HTTP_200_OK)
async def search_doctors_list(search_para: str, token: str = Depends(JWTBearer(user_type=["doctor","view"]))):
    user_data = await search_doctor_name_db(search_para)
    return user_data
    

@router.post('/add/new/', status_code=status.HTTP_200_OK)
async def add_new_doctor_details(user_data : AddUserDoctorDetails = Body(...)): # token: str = Depends(JWTBearer(user_type=["events","view"]))
    dupl_data = await retrieve_user_with_email_id(user_data.email_id)
    if dupl_data:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail='Email Id already exists')
    dupl_data = await retrieve_user_with_phone_no(user_data.phone_no)
    if dupl_data:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail='Phone number already exists')
    user_data = jsonable_encoder(user_data)
    # try:
    resp_data = await add_doctor_user_data_db(data=user_data)
    if resp_data:
        return {
            'user_data': resp_data
        }
    raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='User creation failed')
    # except:
    #     raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='User data updation failed')

@router.put('/update/', status_code=status.HTTP_200_OK)
async def update_doctor_user_data(user_data: UpdateUserDoctorDetails = Body(...), token: str = Depends(JWTBearer(user_type=["doctor","view"]))):
    payload = decodeJWT(token)
    resp = await retrieve_doctor_user_data_db(id=payload['id'])
    if resp:
        user_data = jsonable_encoder(user_data)
        resp = await update_doctor_data_db(payload['id'], user_data)
        if resp:
            return {
                'detail': 'User data updated successfully'
            }
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User data updation failed")
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User data not found")