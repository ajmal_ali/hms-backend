from typing import Optional
from fastapi.param_functions import Form
from pydantic import BaseModel, EmailStr, Field

class AddUserAdminDetails(BaseModel):
    name: str = Form(...)
    designation: str = Form(...)
    email_id: EmailStr = Form(...)
    phone_no: str = Form(...)
    emp_id: str = Form(...)
    password: str = Form(...)

class UpdateUserAdminDetails(BaseModel):
    # id: Optional[str]
    name: Optional[str]
    designation: Optional[str]
    email_id: Optional[EmailStr]
    phone_no: Optional[str]
    emp_id: Optional[str]
    password: Optional[str]

def Admin_user_helper(data) -> dict:
    return {
        "id": str(data["_id"]),
        "name": data["name"],
        "designation": data['designation'],
        "email_id": data['email_id'],
        "phone_no": data['phone_no'],
        "emp_id": data['emp_id'],
        "password": data['password'],
        "user_type": data['user_type'],
    }

def Admin_user_list_helper(data) -> dict:
    return {
        "id": str(data["_id"]),
        "name": data["name"],
        "designation": data['designation'],
        "email_id": data['email_id'],
        "phone_no": data['phone_no'],
        "emp_id": data['emp_id'],
        # "password": data['password'],
        "user_type": data['user_type'],
    }

def Admin_user_search_helper(data) -> dict:
    return {
        "id": str(data["_id"]),
        "name": data["name"],
        # "designation": data['designation'],
        "email_id": data['email_id'],
        "phone_no": data['phone_no'],
        "emp_id": data['emp_id'],
        # "password": data['password'],
        "user_type": data['user_type'],
    }

Admin_user_scope = {
    'Admin': ['update', 'view', 'list', 'notification'],
    'patient': ['update', 'view', 'chat', 'list'],
    'prescription': ['update', 'view', 'add', 'delete']
}