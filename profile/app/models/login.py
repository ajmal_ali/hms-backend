def user_login_helper(data) -> dict:
    return {
        "id": str(data["_id"]),
        "name": data["name"],
        "email_id": data['email_id'],
        "phone_no": data['phone_no'],
        "password": data['password'],
        "user_type": data['user_type'],
        "scope": data['scope'],
    }