from typing import Optional
from fastapi.param_functions import Form
from pydantic import BaseModel, EmailStr, Field

class AddUserPatientDetails(BaseModel):
    name: str = Form(...)
    email_id: EmailStr = Form(...)
    phone_no: str = Form(...)
    address: str = Form(...)
    country: str = Form(...)
    age: str = Form(...)
    gender: str = Form(...)
    dob: str = Form(...)
    hardware_id: str = Form(...)
    password: str = Form(...)
    blood_group: str = Form(...)
    password: str = Form(...)

class UpdateUserPatientDetails(BaseModel):
    # id: Optional[str]
    name: Optional[str]
    email_id: Optional[EmailStr]
    phone_no: Optional[str]
    address: Optional[str]
    country: Optional[str]
    age: Optional[str]
    gender: Optional[str]
    dob: Optional[str]
    hardware_id: Optional[str]
    blood_group: Optional[str]
    password: Optional[str]

def patient_user_helper(data) -> dict:
    return {
        "id": str(data["_id"]),
        "name": data["name"],
        "email_id": data['email_id'],
        "phone_no": data['phone_no'],
        "address": data['address'],
        "country": data["country"],
        "age": data["age"],
        "dob": data["dob"],
        "gender": data["gender"],
        "hardware_id": data['hardware_id'],
        "blood_group": data['blood_group'],
        "password": data['password'],
        "user_type": data['user_type'],
    }

def patient_user_list_helper(data) -> dict:
    return {
        "id": str(data["_id"]),
        "name": data["name"],
        "email_id": data['email_id'],
        "phone_no": data['phone_no'],
        "address": data['address'],
        "country": data["country"],
        "age": data["age"],
        "dob": data["dob"],
        "gender": data["gender"],
        "hardware_id": data['hardware_id'],
        "blood_group": data['blood_group'],
        "user_type": data['user_type'],
    }

def patient_user_search_helper(data) -> dict:
    return {
        "id": str(data["_id"]),
        "name": data["name"],
        "email_id": data['email_id'],
        "phone_no": data['phone_no'],
        "address": data['address'],
        "country": data["country"],
        "age": data["age"],
        "dob": data["dob"],
        "gender": data["gender"],
        "hardware_id": data['hardware_id'],
        "blood_group": data['blood_group'],
        "password": data['password'],
        "user_type": data['user_type'],
    }

patient_user_scope = {
    'patient': ['view', 'chat', 'list'],
    'prescription': ['view']
}