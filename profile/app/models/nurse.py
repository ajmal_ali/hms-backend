from typing import Optional
from fastapi.param_functions import Form
from pydantic import BaseModel, EmailStr, Field

class AddUserNurseDetails(BaseModel):
    name: str = Form(...)
    designation: str = Form(...)
    email_id: EmailStr = Form(...)
    phone_no: str = Form(...)
    address: str = Form(...)
    country: str = Form(...)
    department: str = Form(...)
    emp_id: str = Form(...)
    password: str = Form(...)

class UpdateUserNurseDetails(BaseModel):
    # id: Optional[str]
    name: Optional[str]
    designation: Optional[str]
    email_id: Optional[EmailStr]
    phone_no: Optional[str]
    address: Optional[str]
    country: Optional[str]
    department: Optional[str]
    emp_id: Optional[str]
    password: Optional[str]

def nurse_user_helper(data) -> dict:
    return {
        "id": str(data["_id"]),
        "name": data["name"],
        "designation": data['designation'],
        "email_id": data['email_id'],
        "phone_no": data['phone_no'],
        "address": data['address'],
        "country": data["country"],
        "department": data['department'],
        "emp_id": data['emp_id'],
        "password": data['password'],
        "user_type": data['user_type'],
    }

def nurse_user_list_helper(data) -> dict:
    return {
        "id": str(data["_id"]),
        "name": data["name"],
        "designation": data['designation'],
        "email_id": data['email_id'],
        "phone_no": data['phone_no'],
        "address": data['address'],
        "country": data["country"],
        "department": data['department'],
        "emp_id": data['emp_id'],
        # "password": data['password'],
        "user_type": data['user_type'],
    }

def nurse_user_search_helper(data) -> dict:
    return {
        "id": str(data["_id"]),
        "name": data["name"],
        # "designation": data['designation'],
        "email_id": data['email_id'],
        "phone_no": data['phone_no'],
        # "address": data['address'],
        # "country": data["country"],
        "department": data['department'],
        "emp_id": data['emp_id'],
        # "password": data['password'],
        "user_type": data['user_type'],
    }

nurse_user_scope = {
    'nurse': ['update', 'view', 'list', 'notification'],
    'doctor': ['update', 'view', 'list', 'notification'],
    'patient': ['update', 'view', 'chat', 'list'],
    'prescription': ['update', 'view', 'add', 'delete']
}