import uuid
import random
import string

def generate_random_uid():
    return str(uuid.uuid1())

def generate_rand_no(n=6):
    otp=""
    for i in range(n):
        otp+=str(random.randint(0,9))
    return(otp)

def generate_random_string():  
    letter_count = 20
    digit_count = 12
    str1 = ''.join((random.choice(string.ascii_letters) for x in range(letter_count)))  
    str1 += ''.join((random.choice(string.digits) for x in range(digit_count)))  
    random_list = list(str1)
    random.shuffle(random_list)
    final_string = ''.join(random_list) 
    return final_string 