from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from app.routes.login import router as LoginRouter
from app.routes.doctor import router as DoctorUserRouter
from app.routes.patient import router as PatientUserRouter
from app.routes.nurse import router as NurseRouter

app = FastAPI()

origins = [
    "http://localhost:4200",
    "frontend.rewardsdna.com",
    "http://frontend.rewardsdna.com.s3-website.eu-west-2.amazonaws.com",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(LoginRouter, tags=["Login"], prefix="")
app.include_router(DoctorUserRouter, tags=["Doctor"], prefix="/doctor")
app.include_router(NurseRouter, tags=["Nurse"], prefix="/nurse")
app.include_router(PatientUserRouter, tags=["Patient"], prefix="/patient")


