from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from app.routes.phms import router as PHMSRouter
# from app.routes.file_upload import router as FileRouter
# from app.routes.community_enc import router as CommEncRouter

app = FastAPI()

origins = [
    "http://localhost:4200",
    "frontend.rewardsdna.com",
    "http://frontend.rewardsdna.com.s3-website.eu-west-2.amazonaws.com",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(PHMSRouter, tags=["Patient Health Monitoring System"], prefix="/phms")
# app.include_router(FileRouter, tags=["File"], prefix="/file")
# app.include_router(CommEncRouter, tags=["Community"], prefix="/community")


# app.include_router(PersonalizationRouter, tags=["Profile General"], prefix="")

# app.include_router(Router, tags=["Profile General"], prefix="")


