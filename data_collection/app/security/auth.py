from time import time
from fastapi import HTTPException, Request
from jose import jwt
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials

from app.security.schema import Authdetails
from app.security.security import (
    verify_password,
    SECRET_KEY,
    ALGORITHM,
    ACCESS_TOKEN_EXPIRE_MINUTES
)

class JWTBearer(HTTPBearer):
    user_type = str(None)
    def __init__(self, user_type: list, auto_error: bool = True):
        super(JWTBearer, self).__init__(auto_error=auto_error)
        self.user_type = user_type

    async def __call__(self, request: Request):
        credentials: HTTPAuthorizationCredentials = await super(JWTBearer, self).__call__(request)
        user_type = self.user_type
        if credentials:
            if not credentials.scheme == "Bearer":
                raise HTTPException(status_code=403, detail="Invalid authentication scheme.")
            if self.user_type:
                if not self.verify_jwt(user_type, credentials.credentials):
                    raise HTTPException(status_code=403, detail="Invalid token or expired token.")
            else:
                return HTTPException(status_code=403, detail="Invalid user type")
            return credentials.credentials
        else:
            raise HTTPException(status_code=403, detail="Invalid authorization code.")

    def verify_jwt(self, type: list, jwtoken: str) -> bool:
        isTokenValid: bool = False
        try:
            payload = decodeJWT(jwtoken)
        except:
            payload = None
        if payload:
            # print(payload["scope"])
            scope = payload["scope"]
            if type[0] in list(scope.keys()):
                if not type[1] in scope[type[0]]:
                    raise HTTPException(status_code=403, detail="User do not have the rights")
            else:
                raise HTTPException(status_code=403, detail="User do not have the rights")
            isTokenValid = True
        return isTokenValid

def authenticate_user(authdetails: Authdetails, password : str):
    if not verify_password(authdetails.password, password):
        return False
    return True

def createJWT(user_name: str, scope: dict, id : str, user_type = None, user_status = None, user_email = None, expire_time = None):
    if expire_time:
        payload = {
        "id": id,
        "username": user_name,
        "user_email": user_email,
        "user_type": user_type,
        "user_status": user_status,
        "expires": time() + expire_time * 60,
        "scope": scope
        }
    else:
        payload = {
            "id": id,
            "username": user_name,
            "user_email": user_email,
            "user_type": user_type,
            "user_status": user_status,
            "expires": time() + ACCESS_TOKEN_EXPIRE_MINUTES*60,
            "scope": scope
        }
    token = jwt.encode(payload, SECRET_KEY, algorithm=ALGORITHM)
    return token

# print(
#     createJWT(
#         user_name=None,
#         scope={'user': ['add', 'update', 'view']},
#         id = None,
#         expire_time=100000
#     )
# )

def decodeJWT(token: str) -> dict:
    try:
        decoded_token = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        return decoded_token if decoded_token["expires"] >= time() else None
    except:
        return {}
