from cryptography.fernet import Fernet
import json

def encrypt(message, key):
    key = key.encode()
    message = json_encode(message)
    a = Fernet(key)
    coded_slogan = a.encrypt(message)
    coded_slogan = coded_slogan.decode('utf-8')
    return coded_slogan

def decrypt(encrypted, key):
    encrypted = encrypted.encode('utf-8')
    key = key.encode()
    b = Fernet(key)
    decoded_slogan = b.decrypt(encrypted)
    return json_decode(decoded_slogan)

def json_encode(data):
    encode1 = json.dumps(data)
    encode2 = encode1.encode()
    return encode2

def json_decode(data):
    return json.loads((data))


# data = {
#    "_id":"547896123456716",
#    "mode":"request",
#    "message":"hai",
#    "sender_id":"619baef8521811514235e1fa",
#    "reciever_id":"1002",
#    "msg_type": "message",
#    "content_type": "message",
#    "user_data": {},
#    "created_date":"dd/mm/yy hh:mm:ss"
# }

# key = '2ZhtnYf9PRlwYD8JtjzMGEDdAoYJXt95jaMRxl6qPdc='
# enc_data = encrypt(data, key)
# print(enc_data)
# dec_data = decrypt(enc_data, key)
# print(dec_data)