import rsa
import json

def generate_rsa_key():
    publicKey, privateKey = rsa.newkeys(2048)
    # print(privateKey)
    # print(publicKey)
    return {
        'privateKey': {
            'n': privateKey['n'],
            'e': privateKey['e'],
            'd': privateKey['d'],
            'p': privateKey['p'],
            'q': privateKey['q'],
        },
        'publicKey': {
            'n': publicKey['n'],
            'e': publicKey['e'],
        },
    }

def convert_key_to_rsa_key(key_data):
    privateKey = rsa.PrivateKey(
        int(key_data['privateKey']['n']), int(key_data['privateKey']['e']),
        int(key_data['privateKey']['d']), int(key_data['privateKey']['p']), int(key_data['privateKey']['q'])
    )
    publicKey = rsa.PublicKey(int(key_data['publicKey']['n']), int(key_data['publicKey']['e']))
    return privateKey, publicKey

def encrypt_message(msg_data, publicKey):
    str_data = json.dumps(msg_data)
    encMessage = rsa.encrypt(str_data.encode(), publicKey)
    return encMessage

def decrypt_message(msg_data, privateKey):
    decMessage = rsa.decrypt(msg_data, privateKey).decode()
    dec_data = json.loads(decMessage)
    return dec_data

# data = {
#    "_id":"547896123456716",
#    "mode":"request",
#    "message":"hai",
#    "sender_id":"1000",
#    "reciever_id":"1002",
#    "msg_type": "message",
#    "content_type": "message",
#    "user_data": {},
#    "created_date":"dd/mm/yy hh:mm:ss"
# }

# privateKey, publicKey = convert_key_to_rsa_key(generate_rsa_key())

# encdata = encrypt_message(data, publicKey)
# print(encdata)

# decdata = decrypt_message(encdata, privateKey)
# print(decdata)