from typing import Optional
from fastapi.param_functions import Form
from pydantic import BaseModel, EmailStr, Field

def patient_user_id_user_helper(data) -> dict:
    return {
        "id": str(data["_id"]),
        "hardware_id": data['hardware_id'],
        "name": data['name']
    }