from fastapi import WebSocket
from typing import List

from pydantic.main import BaseModel

# from app.database.chat import add_pending_message_to_user

# from app.database.community_enc import retrieve_crypter_key

from app.security.cypher import (
    encrypt,
    decrypt
)

class ConnectionManager:
    def __init__(self):
        self.active_connections: List[dict] = []
        # self.community_enc_key = await retrieve_crypter_key()

    async def connect(self, connection):
        await connection['websocket'].accept()
        self.active_connections.append(connection)

    def disconnect(self, connection):
        self.active_connections.remove(connection)

    async def send_personal_message(self, data: dict, websocket: WebSocket):
        await websocket.send_json(data)

#request - new messages
#response - status update for messges
#pending - idk if this is needed
    async def send_message(self, data: dict, sender_conn_data):
        pass
        # flag = 0
        # for connection in self.active_connections:
        #     if data['reciever_id'] == connection['user_id']:
        #         # send the msg to reciever
        #         data['mode'] = 'request'
        #         websocket = connection['websocket']
        #         # privatekey, publickey = convert_key_to_rsa_key(connection['crypter_key'])
        #         # data_en = encrypt_message(data, publickey)
        #         data_en = encrypt(data, connection['crypter_key'])
        #         # await websocket.send_text(data_en)
        #         await websocket.send_json({
        #             'msg_type': 'message',
        #             'content': data_en
        #         })
        #         # send the response to sender
        #         data['mode'] = 'response'
        #         data['status'] = 'recieved'
        #         try:
        #             # privatekey, publickey = convert_key_to_rsa_key(sender_conn_data['crypter_key'])
        #             # data_en = encrypt_message(data, publickey)
        #             data_en = encrypt(data, sender_conn_data['crypter_key'])
        #             await sender_conn_data['websocket'].send_json({
        #             'msg_type': 'message',
        #             'content': data_en
        #             })
        #         except:
        #             resp = await add_pending_message_to_user(data)
        #         flag = 1
        #         break
        # if flag==0:
        #     data['status'] = 'sent'
        #     resp = await add_pending_message_to_user(data)
        #     data['mode'] = 'response'
        #     if not resp:
        #         data['status'] = 'failed'
        #     data_en = encrypt(data, sender_conn_data['crypter_key'])
        #     await sender_conn_data['websocket'].send_json({
        #             'msg_type': 'message',
        #             'content': data_en
        #         })

    # async def broadcast(self, data: dict, sender_conn_data):
    #     received_users = []
    #     try:
    #         for connection in self.active_connections:
    #             if connection['user_id'] != data['sender_id']:
    #                 # await connection['websocket'].send_text(data_en)
    #                 await connection['websocket'].send_json({
    #                     'msg_type': 'group',
    #                     'content': data_en
    #                 })
    #                 received_users.append(connection['user_id'])
    #         data['mode'] = 'response'
    #         data['status'] = 'sent'
    #         data['received_users'] = received_users
    #         data_en = encrypt(data, comm_crypt_key)
    #         await sender_conn_data['websocket'].send_json({
    #                     'msg_type': 'group',
    #                     'content': data_en
    #                 })
    #         return data
    #     except:
    #         data['status'] = 'failed'
    #         data_en = encrypt(data, comm_crypt_key)
    #         await sender_conn_data['websocket'].send_json({
    #                     'msg_type': 'group',
    #                     'content': data_en
    #                 })
    #         return None


# class Notifier():
#     def __init__(self):
#         self.connections: List[WebSocket] = []
#         self.generator = self.get_notification_generator()
#         self.user_ids = []

#     async def get_notification_generator(self):
#         while True:
#             message = yield
#             await self._notify(message)

#     async def push(self, msg):
#         await self.generator.asend(msg)

#     async def connect(self, websocket: WebSocket, user_id: str):
#         await websocket.accept()
#         self.connections.append(websocket)
#         self.user_ids.append(user_id)
#         print(self.user_ids)

#     def remove(self, websocket: WebSocket, user_id: str):
#         self.connections.remove(websocket)
#         self.user_ids.remove(user_id)
#         print(self.user_ids)

#     async def notify_all_users(self, type, message):
#         living_connections = []
#         while len(self.connections) > 0:
#             # Looping like this is necessary in case a disconnection is handled
#             # during await websocket.send_text(message)
#             websocket = self.connections.pop()
#             await websocket.send_json({
#                 "type": type,
#                 "data": message
#             })
#             living_connections.append(websocket)
#         self.connections = living_connections

#     # async def notify_selected_users(self, message):
#     #     living_connections = []
#     #     while len(self.connections) > 0:
#     #         # Looping like this is necessary in case a disconnection is handled
#     #         # during await websocket.send_text(message)
#     #         websocket = self.connections.pop()
#     #         await websocket.send_json(message)
#     #         living_connections.append(websocket)
#     #     self.connections = living_connections

#     async def notify_user(self, user_id, message):
#         try:
#             index = self.user_ids.index(user_id)
#             websocket = self.connections[index]
#             await websocket.send_text(message)
#         except:
#             pass

#     async def notify_user_json(self, user_id, data):
#         try:
#             index = self.user_ids.index(user_id)
#             websocket = self.connections[index]
#             await websocket.send_json(data)
#         except:
#             pass

#     async def notify_selected_user(self, user_ids, message):
#             for user_id in user_ids:
#                 try:
#                     if user_id in self.user_ids:
#                         index = self.user_ids.index(user_id)
#                         websocket = self.connections[index]
#                         await websocket.send_text(message)
#                 except:
#                     pass