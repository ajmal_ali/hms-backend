def patient_health_monitor_data_helper(data) -> dict:
    return {
        "id": str(data["_id"]),
        "hardware_id": data["hardware_id"],
        "patient_data": data['patient_data'],
        "time_stamp": data['time_stamp']
    }