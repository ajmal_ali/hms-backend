from bson.objectid import ObjectId
from app.database.config import users_coll

from app.security.security import get_password_hash

from app.models.patient import (
    patient_user_id_user_helper
)

# Retrieve a user_data with a matching ID
async def retrieve_patient_user_data_db(id: str) -> dict:
    user_data = await users_coll.find_one({"hardware_id": id})
    if user_data:
        # if user_data['status'] == 'completed':
            return patient_user_id_user_helper(user_data)
        # return False
    return False