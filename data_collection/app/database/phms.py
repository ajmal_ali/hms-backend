from bson.objectid import ObjectId
from app.database.config import patient_monitor_coll

from app.security.security import get_password_hash

from app.models.phms import patient_health_monitor_data_helper

async def add_patient_monitor_data_db(data: dict) -> dict:
    patient_data = await patient_monitor_coll.insert_one(data)
    new_patient_data = await patient_monitor_coll.find_one({"_id":patient_data.inserted_id})
    return patient_health_monitor_data_helper(new_patient_data)

# # Retrieve a user_data with a matching ID
# async def retrieve_doctor_user_data_db(id: str) -> dict:
#     user_data = await patient_monitor_coll.find_one({"_id": ObjectId(id)})
#     if user_data:
#         # if user_data['status'] == 'completed':
#             return patient_health_monitor_data_helper(user_data)
#         # return False
#     return False

async def retrieve_patient_monitor_data_list_db(hardware_id: str):
    monitor_data_list = []
    async for patient_data in patient_monitor_coll.find({'hardware_id': hardware_id}):
        monitor_data_list.append(patient_health_monitor_data_helper(patient_data))
    return monitor_data_list