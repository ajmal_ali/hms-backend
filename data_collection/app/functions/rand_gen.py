import uuid
import random

def generate_random_uid():
    return str(uuid.uuid1())

def generate_rand_no(n=6):
    otp=""
    for i in range(n):
        otp+=str(random.randint(0,9))
    return(otp)