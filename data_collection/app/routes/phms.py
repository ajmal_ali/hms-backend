from fastapi.exceptions import HTTPException
from fastapi import APIRouter, Body, Depends, status, UploadFile, File, WebSocket, WebSocketDisconnect
from fastapi.encoders import jsonable_encoder

from datetime import datetime

from app.models.websocket import ConnectionManager

from app.security.cypher import (
    json_encode,
    json_decode,
    encrypt,
    decrypt
)

from app.database.phms import (
    retrieve_patient_monitor_data_list_db,
    add_patient_monitor_data_db
)

from app.database.patient import (
    retrieve_patient_user_data_db
)



router = APIRouter()

HardwareConnectionManager = ConnectionManager()
WebAppConnectionManager = ConnectionManager()
WebAppNotifConnectionManager = ConnectionManager()

async def notif_trig(content: str): # sends notification to all
    data = {
        'type': 'notification',
        'content': content
    }
    for conn in WebAppNotifConnectionManager.active_connections:
        try:
            await conn['websocket'].send_json(data)
        except:
            continue

@router.post(path='/view/data/{hardware_id}')
async def view_hardware_data_with_id(hardware_id: str):
    # get the data from the database, so we can have filter here at the same time
    pass

@router.get('get/patient/monitor/data/{hardware_id}', status_code=status.HTTP_200_OK)
async def get_doctor_users_list(hardware_id: str): #token: str = Depends(JWTBearer(user_type=["doctor","list"]))
    try:
        users_data = await retrieve_patient_monitor_data_list_db(hardware_id)
        return users_data
    except:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Retrieval failed")

@router.websocket("/connect/webapp/{user_id}&{hardware_id}")
async def websocket_endpoint_webapp(websocket: WebSocket, user_id: str, hardware_id: str):
    # websocket for webapp which sends the data recieved directly into the webapp, but how to do it
    conn_data_wa = {
        'websocket': websocket,
        'user_id': user_id
    }
    await WebAppConnectionManager.connect(connection=conn_data_wa)
    try:
        while True:
            data = await websocket.receive_text()
            # for conn_data_hw in HardwareConnectionManager.active_connections:
            #     if hardware_id == conn_data_hw['hardware_id']:  
            #         break
            # data = await websocket.receive_text()
            # send data to the webappp
    except WebSocketDisconnect:
        WebAppConnectionManager.disconnect(connection=conn_data_wa)


@router.websocket("/connect/hardware/{hardware_id}")
async def websocket_endpoint_hardware(websocket: WebSocket, hardware_id: str):
    conn_data_hw = {
        'websocket': websocket,
        'hardware_id': hardware_id,
    }
    await HardwareConnectionManager.connect(connection=conn_data_hw)
    try:
        while True:
            data = await websocket.receive_json()
            now = datetime.now()
            # append data into the database here
            if data:
                data = jsonable_encoder(data)
                timestamp = now.strftime("%d/%m/%Y, %H:%M:%S")
                json_data = {
                    'hardware_id': hardware_id,
                    'patient_data': data,
                    'time_stamp': timestamp,
                }
                flag = await add_patient_monitor_data_db(data=json_data)
                # check if hardware_id in patient database, get the user id check the user id connected via websocket then send the data.
                user_data = await retrieve_patient_user_data_db(id=hardware_id)

                if not int(data['SPO2']) > 95:
                    content = "{} has a low blood oxygen level. SPO2 level: {}".format(user_data['name'], data['SPO2'])
                    await notif_trig(content)

                if ((int(data['bpm']) > 100) or (int(data['bpm']) < 60)):
                    content = "{} has an abnormal heart rate. Heart Rate: {}".format(user_data['name'], data['bpm'])
                    await notif_trig(content)

                if ((float(data['tempC']) > 37.2) or (float(data['tempC']) < 36.1)):
                    content = "{} has an abnormal body temperature. Body Temperature: {}".format(user_data['name'], data['tempC'])
                    await notif_trig(content)

                if user_data:
                    for webapp_conn in WebAppConnectionManager.active_connections:
                        if user_data['id'] in webapp_conn['user_id']:
                            await webapp_conn['websocket'].send_json(data)
    except WebSocketDisconnect:
        HardwareConnectionManager.disconnect(connection=conn_data_hw)

@router.websocket("/connect/webapp/notification/{user_id}")
async def websocket_endpoint_webapp(websocket: WebSocket, user_id: str):
    # websocket for webapp which sends the data recieved directly into the webapp, but how to do it
    conn_data_wa = {
        'websocket': websocket,
        'user_id': user_id
    }
    await WebAppNotifConnectionManager.connect(connection=conn_data_wa)
    try:
        while True:
            data = await websocket.receive_text()
    except WebSocketDisconnect:
        WebAppNotifConnectionManager.disconnect(connection=conn_data_wa)


